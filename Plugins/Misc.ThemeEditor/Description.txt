﻿Group: Misc
FriendlyName: Theme Editor
SystemName: Misc.ThemeEditor
Version: 1.00
SupportedVersions: 3.90
Author: nop4you.com
DisplayOrder: 1
FileName: Nop4you.Plugin.Misc.Theme.dll
Description: Plugin allows to edit your theme.