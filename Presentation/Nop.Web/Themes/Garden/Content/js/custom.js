﻿$(document).ready(function () {


    $('.featured-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 5000
    });

    $(".mms").elevateZoom({
        zoomType: "lens",
        lensShape: "rectangle",
        lensSize: 150,

    });


    function displayZoomLens() {
        $(".zoomContainer").css("display", "block");
        $(".zoomContainer").css("cursor", "zoom-out");
        $(".picture-main").addClass("new_picturemain");
        $(".picture-main").removeClass("picture-main");
    }

    function hideZoomLens() {
        $(".zoomContainer").css("cursor", "zoom-in");
        $(".zoomContainer").css("display", "none");
        $(".new_picturemain").addClass("picture-main");
        $(".new_picturemain").removeClass("new_picturemain");
    }
});

